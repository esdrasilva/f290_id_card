import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomeApp(),
  ));
}

class HomeApp extends StatelessWidget {
  _launchUrl() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Falha ao abrir url: $url';
    }
  }

  _launchDialer() async {
    const tel = 'tel:+551935431000';
    if (await canLaunch(tel)) {
      await launch(tel);
    } else {
      throw 'Não pode fazer ligacao para $tel.';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade800,
      body: SafeArea(
        child: Card(
          margin: EdgeInsets.all(25),
          elevation: 15,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage("images/ahsoka_tano.png"),
              ),
              Text(
                "Ahsoka Tano",
                style: TextStyle(
                    fontFamily: 'Pacifico',
                    color: Color(0xFFC2185B),
                    fontSize: 36,
                    fontWeight: FontWeight.w800),
              ),
              Text(
                "Comandante Jedi",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Anton',
                    fontSize: 16,
                    color: Color(0xFFC2185B),
                    textBaseline: TextBaseline.ideographic),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 10),
                child: Divider(
                  color: Colors.grey.shade500,
                ),
              ),
              FlatButton(                
                onPressed: () {
                  _launchUrl();
                },
                child: Card(                  
                  elevation: 5,
                  child: ListTile(                    
                    leading: Icon(
                      FontAwesomeIcons.linkedin,
                      color: Color(0xFFC2185B),
                      size: 24,
                    ),
                    title: Text("linkedin.com/ahsokatano"),
                  ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _launchDialer();
                },
                child: Card(
                  elevation: 5,
                  child: ListTile(
                    leading: Icon(Icons.phone, color: Color(0xFFC2185B)),
                    title: Text("+55 (19) 3543-1000"),
                  ),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Image.asset(
                    "images/fatec.png",
                    width: 160,
                    height: 100,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
